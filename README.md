# Challenge

API Challenge

[![Built with Cookiecutter Django](https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg?logo=cookiecutter)](https://github.com/cookiecutter/cookiecutter-django/)
[![Black code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

License: MIT

## Settings

Moved to [settings](http://cookiecutter-django.readthedocs.io/en/latest/settings.html).

## Basic Commands


- To define COMPOSE_FILE:

        $ export COMPOSE_FILE=local.yml
- To build run:

        $ docker-compose build
- To build run:

        $ docker-compose up


- To run command for import .csv file in database

        $ docker-compose run --rm django manage.py import_csv --path path_to_file




### Test coverage

To run the tests and check your test coverage:

    $ docker-compose run --rm django pytest --cov


### Endpoint Documentation

- GET    /reports/{campaign}/   
returns count for given campaign



You could provide query strings to url to filter campaign data by datetime
- GET  
/reports/{campaign}/?start_date=2021-11-07 03:10:00&end_date=2021-11-07 03:30:00


And also you can provide just start_date or end_date


- GET   /reports/12222/?start_date=2021-11-06 03:10:00

- GET   /reports/12222/?end_date=2021-11-06 03:10:00

If you
    provide just start_date you will get number of clicks for campaign from that date until now, on the other side if you provide just
    end_date you will get number of clicks from when we have records until end_date

