from django.core.management.base import BaseCommand
from challenge.reports.models import Record
import pandas as pd


class Command(BaseCommand):
    help = "Import data from csv file into Record model"

    def add_arguments(self, parser):
        parser.add_argument("--path", type=str)

    def handle(self, *args, **kwargs):
        path = kwargs["path"]
        df = pd.read_csv(path)
        df = df.dropna(subset=["id"])
        records_for_create = []
        for index, row in df.iterrows():
            records_for_create.append(Record(
                external_id=row["id"],
                timestamp=row["timestamp"],
                type=row["type"],
                campaign=row["campaign"],
                banner=row["banner"],
                content_unit=row["content_unit"],
                network=row["network"],
                browser=row["browser"],
                operating_system=row["operating_system"],
                country=row["country"],
                state=row["state"],
                city=row["city"],
            ))
        Record.objects.bulk_create(records_for_create, batch_size=1000)
