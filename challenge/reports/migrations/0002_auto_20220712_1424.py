# Generated by Django 3.2.14 on 2022-07-12 14:24

from django.db import migrations, models
import unixtimestampfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='record',
            name='browser',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='record',
            name='banner',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='record',
            name='campaign',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='record',
            name='city',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='record',
            name='content_unit',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='record',
            name='country',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='record',
            name='network',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='record',
            name='operating_system',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='record',
            name='state',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='record',
            name='timestamp',
            field=unixtimestampfield.fields.UnixTimeStampField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='record',
            name='type',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
