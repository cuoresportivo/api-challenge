import pytest
from model_bakery import baker
from challenge.reports.models import Record


@pytest.mark.django_db
def test_client_can_get_campaign_reports_endpoint(client, django_user_model):
    """Test if client can get reports view with just campaign argument"""
    get_response = client.get("/reports/12131/")
    assert get_response.status_code == 200

    assert get_response.data["count"] == 0

    # Create two records for campaign 12131
    record_one = baker.make(Record, campaign=12131)
    record_two = baker.make(Record, campaign=12131)
    record_three = baker.make(Record, campaign=22223)

    second_get_response = client.get("/reports/12131/")
    assert second_get_response.status_code == 200
    assert second_get_response.data["count"] == 2


@pytest.mark.django_db
def test_client_can_get_report_campaign_with_start_date(client, django_user_model):
    """Test if client can get reports when he provides campaign and start_date"""
    get_response = client.get("/reports/12222/?start_date=2021-11-06 03:10:00")
    assert get_response.status_code == 200

    # Create two records for campaign 12222 with timestamp which is after start date query string
    record_one_after = baker.make(Record, campaign=12222, timestamp=1657792543)
    record_two_after = baker.make(Record, campaign=12222, timestamp=1657792543)
    record_three_before = baker.make(Record, campaign=12222, timestamp=1597398943)
    record_other_campaign = baker.make(Record, campaign=12112, timestamp=1657792543)

    second_get_response = client.get("/reports/12222/?start_date=2021-11-06 03:10:00")
    assert second_get_response.status_code == 200
    assert second_get_response.data["count"] == 2

    wrong_request = client.get("/reports/12222/?start_date=1212412")
    assert wrong_request.status_code == 400


@pytest.mark.django_db
def test_client_can_get_report_campaign_with_end_date(client, django_user_model):
    """Test if client can get reports when he provides campaign and end_date"""
    get_response = client.get("/reports/11111/?end_date=2022-07-20 11:22:00")
    assert get_response.status_code == 200

    # Create three records for campaign 11111 with two timestamp before end date and one after
    # Last record is in given date but is from other campaign
    record_one_before = baker.make(Record, campaign=11111, timestamp=1657792543)
    record_two_before = baker.make(Record, campaign=11111, timestamp=1657792543)
    record_three_after = baker.make(Record, campaign=11111, timestamp=1660470943)
    record_other_campaign = baker.make(Record, campaign=111333, timestamp=1657792543)

    second_get_response = client.get("/reports/11111/?end_date=2022-07-20 11:22:00")
    assert second_get_response.status_code == 200
    assert second_get_response.data["count"] == 2

    wrong_request = client.get("/reports/11111/?end_date=2022-07-20")
    assert wrong_request.status_code == 400


@pytest.mark.django_db
def test_client_can_get_report_campaign_start_date_end_date(client, django_user_model):
    """Test if client can get reports when he provides kwarg campaign and query strings start date and end date"""
    get_response = client.get(
        "/reports/33333/?start_date=2020-11-06 03:10:00&end_date=2022-07-20 11:22:00"
    )
    assert get_response.status_code == 200

    # Create three record , two between given dates but one from given campaign
    record_one_between = baker.make(Record, campaign=33333, timestamp=1626258507)
    record_two = baker.make(Record, campaign=33333, timestamp=1563100107)
    record_three_other_campaign = baker.make(
        Record, campaign=32112, timestamp=1626258507
    )

    second_response = client.get(
        "/reports/33333/?start_date=2020-11-06 03:10:00&end_date=2022-07-20 11:22:00"
    )
    assert second_response.status_code == 200
    assert second_response.data["count"] == 1

    wrong_request = client.get(
        "/reports/33333/?start_date=2020- 03:10:00&end_date=2022-070 11:22:00"
    )
    assert wrong_request.status_code == 400
