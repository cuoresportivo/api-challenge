from rest_framework.views import APIView
from challenge.reports.models import Record
from rest_framework.response import Response
from datetime import datetime, timezone
from rest_framework import status


def resolve_time(time):
    format_time = datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
    unix_time = format_time.replace(tzinfo=timezone.utc).timestamp()
    return unix_time


class ReportView(APIView):
    """View which will return number of clicks for given campaign. You can provide to endpoint campaign as a kwarg.
    You can provide start_date and end_date as query string to see number of clicks between this two dates, also if you
    provide just start_date you will get number of clicks from that date until now, on the other side if you provide just
    end_date you will get number of clicks from when we have records until end_date"""

    def get(self, request, *args, **kwargs):

        campaign = kwargs.get("campaign")
        start_date = self.request.GET.get("start_date", None)
        end_date = self.request.GET.get("end_date", None)

        # Situation when we have campaign kwarg and start_date and end_date query strings
        if campaign and start_date and end_date:
            try:
                start_time = resolve_time(start_date)
                end_time = resolve_time(end_date)
            except ValueError:
                return Response(
                    {"message": "Bad datetime format"},
                    status=status.HTTP_400_BAD_REQUEST,
                )

            number = (
                Record.objects.filter(campaign=campaign)
                .filter(timestamp__gte=start_time)
                .filter(timestamp__lte=end_time)
                .count()
            )

        # Situation when we have only campaign kwarg and start_date query string
        elif campaign and start_date and not end_date:
            try:
                start_time = resolve_time(start_date)
            except ValueError:
                return Response(
                    {"message": "Bad datetime format"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            number = (
                Record.objects.filter(campaign=campaign)
                .filter(timestamp__gte=start_time)
                .count()
            )
            # Situation when we have only campaign kwarg and end_date query string
        elif campaign and end_date and not start_date:
            try:
                end_time = resolve_time(end_date)
            except ValueError:
                return Response(
                    {"message": "Bad datetime format"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            number = (
                Record.objects.filter(campaign=campaign)
                .filter(timestamp__lte=end_time)
                .count()
            )

        else:
            number = Record.objects.filter(campaign=campaign).count()

        content = {"count": number}
        return Response(content)
