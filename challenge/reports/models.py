from django.db import models
from unixtimestampfield.fields import UnixTimeStampField


class Record(models.Model):
    external_id = models.CharField(max_length=50)
    timestamp = UnixTimeStampField(blank=True, null=True)
    type = models.CharField(max_length=100, blank=True, null=True)
    campaign = models.IntegerField(blank=True, null=True)
    banner = models.IntegerField(blank=True, null=True)
    content_unit = models.IntegerField(blank=True, null=True)
    network = models.IntegerField(blank=True, null=True)
    browser = models.IntegerField(blank=True, null=True)
    operating_system = models.IntegerField(blank=True, null=True)
    country = models.IntegerField(blank=True, null=True)
    state = models.IntegerField(blank=True, null=True)
    city = models.IntegerField(blank=True, null=True)

