from rest_framework import routers
from django.urls import path
from challenge.reports.views import ReportView

urlpatterns = [
    path("reports/<int:campaign>/", ReportView.as_view()),
]
